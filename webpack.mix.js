const mix = require('laravel-mix');

// mix.webpackConfig({
//   resolve: {
//     extensions: ['.js', '.vue', '.json'],
//     alias: {
//       'component': __dirname + '/resources/js'
//     },
//   },
// })

mix.webpackConfig({
   resolve: {
       alias: {
         //   '@models': path.resolve(__dirname, 'resources/js/models/'),
         //   '@services': path.resolve(__dirname, 'resources/js/services/'),
         //   '@sections': path.resolve(__dirname, 'resources/js/sections/'),
           "assets": path.resolve(__dirname, 'resources/js/assets'),
           "components": path.resolve(__dirname, 'resources/js/components'),
           "views": path.resolve(__dirname, 'resources/js/views'),
       }
   }
});
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.browserSync('localhost:8000')
mix.react('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

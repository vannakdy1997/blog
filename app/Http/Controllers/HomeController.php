<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(Request $res){
        $home = DB::table('user')->get();
        return response()->json([
            'home' => $home
        ]);
        
    }
}

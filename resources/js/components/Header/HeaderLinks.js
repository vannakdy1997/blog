/*eslint-disable*/
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import { Apps, CloudDownload } from "@material-ui/icons";

// core components
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (

    <List className={classes.list}>

      <ListItem className={classes.listItem}>
        <Button
          color="transparent"
          className={classes.navLink}
        >
           <Link to="/demosdSADasd">Deno</Link>
        </Button>
      </ListItem>

      <ListItem className={classes.listItem}>
        <Button
          color="transparent"
          className={classes.navLink}
        >
          <Link to="/home" style={{color:'white'}}>ទំព័រដើម</Link>
        </Button>
      </ListItem>

      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          buttonText="អំពីយើង"
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          // buttonIcon={Apps}
          dropdownList={[
            <Link to="/about_us" className={classes.dropdownLink}>អំពីយើង</Link>,
            // <Link to="/" className={classes.dropdownLink}>មេធីវី​​ និង សមាជិក-សមាជិកា</Link>,
            <Link to="/about_us_structure" className={classes.dropdownLink}>រចនាសម្ព័ន្ធ</Link>,
          ]}
        />
      </ListItem>

      <ListItem className={classes.listItem}>
        <Button color="transparent" className={classes.navLink} >
          <Link to="/service" style={{color:'white'}}>សេវាកម្ម</Link>
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button color="transparent" className={classes.navLink} >
          <Link to="/company_relate" style={{color:'white'}}>ក្រុមហ៊ុនដៃគូ</Link>
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button color="transparent" className={classes.navLink} >
          <Link to="/action" style={{color:'white'}}>សកម្មភាព</Link>
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href="http://localhost:3000/"
          color="transparent"
          target="_blank"
          className={classes.navLink}
        >
          ការស្រាវជ្រាវច្បាប់
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button color="transparent" className={classes.navLink} >
          <Link to="/contact" style={{color:'white'}}>ទំនាក់ទំនង</Link>
        </Button>
      </ListItem>

      {/* <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          buttonText="ធនធាន"
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          // buttonIcon={Apps}
          dropdownList={[
            <Link to="/" className={classes.dropdownLink}>អត្តបទ</Link>,
            <Link to="/" className={classes.dropdownLink}>វីដេអូ</Link>,
          ]}
        />
      </ListItem> */}

      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-twitter"
          title="Follow us on twitter"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            href="https://twitter.com/CreativeTim?ref=creativetim"
            target="_blank"
            color="transparent"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-twitter"} />
          </Button>
        </Tooltip>
      </ListItem>

      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-facebook"
          title="Follow us on facebook"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href="https://www.facebook.com/CreativeTim?ref=creativetim"
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-facebook"} />
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-tooltip"
          title="Follow us on instagram"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href="https://www.instagram.com/CreativeTimOfficial?ref=creativetim"
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-instagram"} />
          </Button>
        </Tooltip>
      </ListItem>
    </List>
  );
}

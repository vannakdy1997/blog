import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
export default function LoadingBackDrop(props){
    return(
        <Backdrop style={{zIndex:10,color:'green'}} open={props.loading} >
            <CircularProgress color="inherit" />
        </Backdrop>
    )
}
import React from 'react';
import axios from 'axios';
import MasterPage from '../MasterPage.js';
import LoadingBackDrop from 'components/Loading/LoadingBackDrop.js'
class HomePage extends React.PureComponent{
    constructor(props){
        super(props);
        this.state={
            loading:true,
            list:[]
        }
        // this.getList = this.getList.bind(this);
    }

    componentWillMount(){
        this.getList();
    }

    getList(){
        axios.get('/home').then(res=>{
            var data  = res.data;
            console.log(data);
            this.setState({
                list:data.home,
                loading:false
            });
        });

    }

    render(){
         const {loading}=this.state;
        return(
            <>
                <LoadingBackDrop loading={loading}/>
                <h3>Home pageddd</h3>
                {/* {
                    this.state.list.map((item,index)=>
                        <h1 key={index}>{item.name}</h1>
                    )  
                } */}
            </>
        )
    }
}

export default HomePage;
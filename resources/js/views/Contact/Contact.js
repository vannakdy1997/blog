import React from 'react';
import LoadingBackDrop from 'components/Loading/LoadingBackDrop.js'
import MasterPage from 'views/MasterPage.js';
class Contact extends React.PureComponent{
    constructor(props){
        super(props);
        this.state={
            loading:true,
        }
    }

    componentWillMount(){
        setTimeout(() => {
            this.setState({
                loading:false
            })
        }, 1000);
    }

    render(){
         const {loading}=this.state;
        return(
            <>
                <LoadingBackDrop loading={loading}/>
                <h3>Contact</h3>
            </>
        )
    }
}

export default Contact;
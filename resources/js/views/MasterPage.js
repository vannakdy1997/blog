// import React from 'react'
// export default function A(){
//   return(
//     <div>
//       <h1>yyyy</h1>
//     </div>
//   )
// }

import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import { Link } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import Parallax from "components/Parallax/Parallax.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import SlideHomePage from "views/Components/Sections/SlideHomePage.js";


import styles from "assets/jss/material-kit-react/views/components.js";

const useStyles = makeStyles(styles);

export default function Components(props) {
  
  const classes = useStyles();
  const loading = window.$loading;
  // const [loading, setLoadin] = React.useState(true);

  const { ...rest } = props;
  return (
    <div>
      <Header
        brand="KaoSeyha"
        rightLinks={<HeaderLinks />}
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 10,
          color: "success",
        }}
        {...rest}
      />
      <Parallax  
        // image={require("assets/img/law2.jpg")}
        // image={<SlideHomePage />}
      >
        <div style={{width:"100%",height:'100%'}}>
          <SlideHomePage />
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.sections}>
          <div className={classes.container}>
            <div className={classes.title}>
              {props.children}
            </div>
          </div>
        </div>
        <Footer />
      </div>
      {/* <Footer /> */}
    </div>
  );
}

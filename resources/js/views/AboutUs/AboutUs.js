import React from 'react';
import LoadingBackDrop from 'components/Loading/LoadingBackDrop.js'
import MasterPage from 'views/MasterPage.js';
class AboutUs extends React.PureComponent{
    constructor(props){
        super(props);
        this.state={
            loading:true,
            list:[]
        }
    }

    componentWillMount(){
        this.getList();
    }

    getList(){
        axios.get('/home').then(res=>{
            var data  = res.data;
            console.log(data);
            this.setState({
                list:data.home,
                loading:false
            });
        });

    }

    render(){
         const {loading}=this.state;
        return(
            <>
                <LoadingBackDrop loading={loading}/>
                <h3>About Us</h3>
                {
                    this.state.list.map((item,index)=>
                        <h1 key={index}>{item.name}</h1>
                    )  
                }
            </>
        )
    }
}

export default AboutUs;
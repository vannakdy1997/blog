import React from 'react';
import LoadingBackDrop from 'components/Loading/LoadingBackDrop.js'
class AboutUsStructure extends React.PureComponent{
    constructor(props){
        super(props);
        this.state={
            loading:true,
        }
    }

    componentWillMount(){
        setTimeout(() => {
            this.setState({
                loading:false
            })
        }, 1000);
    }

    render(){
         const {loading}=this.state;
        return(
            <>
                <LoadingBackDrop loading={loading}/>
                <h3>About Us Stucture</h3>
            </>
        )
    }
}

export default AboutUsStructure;
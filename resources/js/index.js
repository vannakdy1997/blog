import React from "react";
import ReactDOM from "react-dom";
import "assets/scss/material-kit-react.scss?v=1.8.0";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, BrowserRouter } from "react-router-dom";
import MasterPage from 'views/MasterPage.js';
import ScrollToTop from 'components/Scroll/ScrollToTop.js'
import HomePage from 'views/HomePage/HomePage.js'

import AboutUs from 'views/AboutUs/AboutUs.js'
import AboutUsStructure from 'views/AboutUs/AboutUsStructure.js'

import Service from 'views/Service/Service.js'
import CompanyRelate from 'views/Company/CompanyRelate.js'
import Action from 'views/Action/Action.js'
import Contact from 'views/Contact/Contact.js'

import DemoComponent from "views/Components/DemoComponent.js";
import LandingPage from "views/LandingPage/LandingPage.js";
import ProfilePage from "views/ProfilePage/ProfilePage.js";
import LoginPage from "views/LoginPage/LoginPage.js";

var hist = createBrowserHistory();
//global variabl
window.$loading = false;
ReactDOM.render(
  <Router history={hist}>
    <BrowserRouter>
    <Switch>
      <MasterPage>
        <ScrollToTop>
          <Route path="/user">
              <A/>
          </Route>
          <Route path="/landing-page" component={LandingPage} />
          <Route path="/profile-page" component={ProfilePage} />
          <Route path="/login-page" component={LoginPage} />

          <Route path="/contact" component={Contact} />
          <Route path="/law_searching" component={DemoComponent} />
          <Route path="/action" component={Action} />
          <Route path="/company_relate" component={CompanyRelate} />
          <Route path="/service" component={Service} />
          <Route path="/about_us_structure" component={AboutUsStructure} />
          <Route path="/about_us" component={AboutUs} />
          <Route path="/home" component={HomePage} />
          <Route exact={true} path="/" component={HomePage} />
          {/* <Route component={<NotFound/>}/> */}
           {/* <Route path='*' exact={true}>
            <NotFound/>
           </Route> */}
        </ScrollToTop>
      </MasterPage>
    </Switch>
    </BrowserRouter>
  </Router>,
  document.getElementById("root")
);

function NotFound(){
  return(
    <div>
      <h1>Route Not Found</h1>
    </div>
  )
}

function A(){
  return(
    <div>
      <h1>Route Not Found</h1>
    </div>
  )
}

// import ReactDOM from "react-dom";
// import { createBrowserHistory } from "history";
// import { Router, Route, Switch } from "react-router-dom";

// import "assets/scss/material-kit-react.scss?v=1.8.0";

// pages for this product
// import Components from "views/Components/Components.js";
// import LandingPage from "views/LandingPage/LandingPage.js";
// import ProfilePage from "views/ProfilePage/ProfilePage.js";
// import LoginPage from "views/LoginPage/LoginPage.js";

// var hist = createBrowserHistory();

// ReactDOM.render(
//   <Router history={hist}>
//     <Switch>
//       <Route path="/landing-page" component={LandingPage} />
//       <Route path="/profile-page" component={ProfilePage} />
//       <Route path="/login-page" component={LoginPage} />
//       <Route path="/" component={Components} />
//     </Switch>
//   </Router>,
//   document.getElementById("root")
// );
